import React, { Component } from 'react'
import moment from 'moment'
import ProjectList from './components/ProjectList'
import ProjectEditor from './components/ProjectEditor'
import 'react-datepicker/dist/react-datepicker.css'
import firebase from './library/firebase'
import initialPhase from './library/initialPhase'
// import './App.css';

function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1
        while (k--) {
            arr.push(undefined)
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0])
    return arr // for testing
}

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showingProjectList: true,
            projects: [],
            startDate: moment(),
            complexity: 3,
            siteTitle: 'New Site',
            phases: [],
            activeProject: false,
            everythingHidden: true
        }
        this.setPhaseVal = this.setPhaseVal.bind(this)
        this.toggleActive = this.toggleActive.bind(this)
        this.makeNewProject = this.makeNewProject.bind(this)
        this.openProject = this.openProject.bind(this)
        this.updateProject = this.updateProject.bind(this)
        this.deleteProject = this.deleteProject.bind(this)
        this.showProjectList = this.showProjectList.bind(this)
        this.updateProjectPhase = this.updateProjectPhase.bind(this)
        this.replaceInitialValues = this.replaceInitialValues.bind(this)
        this.reorderPhases = this.reorderPhases.bind(this)
    }

    componentDidMount() {
        const projects = firebase.database().ref('projects')
        projects.on('value', snapshot => {
            let items = snapshot.val()
            let projects = []
            for (let item in items) {
                projects.push({
                    id: item,
                    startDate: items[item].startDate,
                    complexity: items[item].complexity,
                    siteTitle: items[item].siteTitle,
                    phases: items[item].phases
                })
            }
            this.setState({
                projects
            })
        })
    }

    showProjectList() {
        this.setState({ showingProjectList: true, activeProject: false })
    }

    makeNewProject() {
        firebase
            .database()
            .ref('projects')
            .push(initialPhase)
            .then(snap => {
                this.openProject(snap.key)
            })
    }

    deleteProject(id) {
        if (window.confirm('Are you sure you want to delete this project?')) {
            this.setState({ showingProjectList: true }, () => {
                this.setState({ activeProject: false })
                firebase
                    .database()
                    .ref('projects')
                    .child(id)
                    .remove()
            })
        } else {
            return false
        }
    }

    openProject(projectID) {
        this.setState({ activeProject: projectID }, () => {
            this.setState({ showingProjectList: false })
        })
    }

    toggleActive(title) {
        const { phases } = this.state
        const phase = phases.filter(x => x.title === title)[0]
        phase.active = !phase.active
        this.setState({ phases })
    }

    updateProject(id, prop, val) {
        const o = {}
        o[prop] = val
        const projectRef = firebase.database().ref(`projects/${id}`)
        projectRef.update(o)
    }

    updateProjectPhase(projectID, phaseID, phaseProp, phaseValue) {
        const { phases } = this.state.projects.filter(
            x => x.id === this.state.activeProject
        )[0]
        const projectRef = firebase.database().ref(`projects/${projectID}`)
        phases[phaseID][phaseProp] = phaseValue
        projectRef.update({ phases })
    }

    setPhaseVal(title, val) {
        const { phases } = this.state
        phases.filter(x => x.title === title)[0].value = val
        this.setState({ phases })
    }

    replaceInitialValues() {
        let currentProject = this.state.projects.filter(
            x => x.id === this.state.activeProject
        )
        if (
            currentProject.length &&
            window.confirm(
                'Are you sure you wish to reset all values to their initial state?'
            )
        ) {
            currentProject = currentProject[0]
            let newProject = Object.assign(initialPhase, {
                siteTitle: currentProject.siteTitle,
                startDate: currentProject.startDate
            })
            newProject.phases.forEach(p => {
                const foundPhase = initialPhase.phases.filter(
                    x => x.title === p.title
                )
                if (foundPhase.length) {
                    p.value = foundPhase[0].value
                    p.active = foundPhase[0].active
                }
            })
            const projectRef = firebase
                .database()
                .ref(`projects/${this.state.activeProject}`)
            projectRef.update(newProject)
        }
    }

    reorderPhases(oldPosition, newPosition) {
        console.log(`reordering phases from ${oldPosition} to ${newPosition}`)
        let { phases } = this.state.projects.filter(
            x => x.id === this.state.activeProject
        )[0]
        phases = array_move(phases, oldPosition, newPosition)
        this.updateProject(this.state.activeProject, 'phases', phases)
    }

    render() {
        const {
            complexity,
            startDate,
            siteTitle,
            showingProjectList,
            activeProject,
            projects
        } = this.state
        let d = moment(startDate)
        return (
            <div className={`App`}>
                {this.state.everythingHidden ? (
                    <input
                        type="password"
                        onKeyDown={e => {
                            if (e.key === 'Enter') {
                                if (e.target.value === 'Br@nd1ng!') {
                                    this.setState({ everythingHidden: false })
                                }
                            }
                        }}
                    />
                ) : showingProjectList && !activeProject ? (
                    <ProjectList
                        projects={projects}
                        makeNewProject={this.makeNewProject}
                        openProject={this.openProject}
                    />
                ) : (
                    <ProjectEditor
                        updateProject={this.updateProject}
                        updateProjectPhase={this.updateProjectPhase}
                        toggleActive={this.toggleActive}
                        setPhaseVal={this.setPhaseVal}
                        deleteProject={this.deleteProject}
                        showProjectList={this.showProjectList}
                        replaceInitialValues={this.replaceInitialValues}
                        initialPhase={initialPhase}
                        reorderPhases={this.reorderPhases}
                        activeProject={
                            this.state.projects.filter(
                                x => x.id === activeProject
                            )[0]
                        }
                    />
                )}
            </div>
        )
    }
}

export default App
