import React, { Component } from 'react'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import business from 'moment-business'
import FaPlus from 'react-icons/lib/fa/plus'
import FaSort from 'react-icons/lib/fa/sort'
import AutosizeInput from 'react-input-autosize'
import initialPhase from '../library/initialPhase'
import { Container, Draggable } from 'react-smooth-dnd'

const AddPhase = props => {
    return (
        <div className="phase">
            <span style={{ marginRight: 'auto' }}>Add Phase</span>
            <div className="column">
                <button>
                    <FaPlus />
                </button>
            </div>
            <div className="column">
                <input type="text" />
            </div>
            <div className="column">
                <select name="fixed-or-relative" id="">
                    <option value="fixed">Fixed</option>
                    <option value="relative">Relative</option>
                </select>
            </div>
        </div>
    )
}

class ProjectEditor extends Component {
    constructor(props) {
        super(props)
        this.finishDrag = this.finishDrag.bind(this)
    }
    finishDrag(dropResult) {
        this.props.reorderPhases(dropResult.removedIndex, dropResult.addedIndex)
    }
    render() {
        const {
            activeProject,
            updateProject,
            deleteProject,
            showProjectList,
            updateProjectPhase,
            replaceInitialValues,
            initialPhase,
            reorderPhases
        } = this.props
        let d = moment(activeProject.startDate)
        return (
            <div className="project-editor">
                <button onClick={showProjectList}>Back to Project List</button>
                <div className="container">
                    <div className="options">
                        <div className="option title">
                            <h4>Title</h4>
                            <AutosizeInput
                                value={activeProject.siteTitle}
                                onChange={e => {
                                    updateProject(
                                        activeProject.id,
                                        'siteTitle',
                                        e.target.value
                                    )
                                }}
                            />
                        </div>
                        <div className="option startDate">
                            <h4>Start Date</h4>
                            <DatePicker
                                selected={moment(
                                    this.props.activeProject.startDate
                                )}
                                dateFormat="MMMM Do"
                                onChange={date => {
                                    updateProject(
                                        activeProject.id,
                                        'startDate',
                                        date.format()
                                    )
                                }}
                            />
                        </div>
                        <div className="option">
                            <h4>Complexity</h4>
                            <input
                                type="range"
                                value={activeProject.complexity}
                                min={1}
                                max={10}
                                step={0.05}
                                onChange={e => {
                                    updateProject(
                                        activeProject.id,
                                        'complexity',
                                        e.target.value
                                    )
                                }}
                            />
                            <button
                                onClick={() => {
                                    updateProject(
                                        activeProject.id,
                                        'complexity',
                                        initialPhase.complexity
                                    )
                                }}
                            >
                                Reset
                            </button>
                            <span>{activeProject.complexity}</span>
                        </div>
                    </div>

                    <div className="phases">
                        <div className="phase thead">
                            <div className="column handle" />
                            <div className="column title">Phase</div>
                            <div className="column active">Active</div>
                            <div className="column drag">Weight</div>
                            <div className="column reset">Default</div>
                            <div className="column value">Time</div>
                            <div className="column date">Start Date</div>
                            <div className="column date">Finish Date</div>
                        </div>
                        <Container
                            onDrop={this.finishDrag}
                            dragHandleSelector={'.column.handle'}
                        >
                            {this.props.activeProject.phases.map((phase, i) => {
                                const realValue =
                                    phase.type === 'fixed'
                                        ? phase.value
                                        : phase.value * activeProject.complexity
                                const start_date = moment(d)
                                if (phase.active) {
                                    d = business.addWeekDays(d, realValue)
                                }

                                const v =
                                    phase.type === 'fixed'
                                        ? phase.value
                                        : phase.value * activeProject.complexity
                                return (
                                    <Draggable>
                                        <div
                                            className={`phase ${phase.type} ${
                                                phase.active
                                                    ? 'active'
                                                    : 'disabled'
                                            }`}
                                        >
                                            <div className="column handle">
                                                <FaSort />
                                            </div>
                                            <div className="column title">
                                                <AutosizeInput
                                                    value={phase.title}
                                                    onChange={e => {
                                                        updateProjectPhase(
                                                            activeProject.id,
                                                            i,
                                                            'title',
                                                            e.target.value
                                                        )
                                                    }}
                                                />
                                            </div>
                                            <div className="column active">
                                                <input
                                                    type="checkbox"
                                                    checked={phase.active}
                                                    onChange={e => {
                                                        updateProjectPhase(
                                                            activeProject.id,
                                                            i,
                                                            'active',
                                                            e.target.checked
                                                        )
                                                    }}
                                                />
                                            </div>

                                            <div className="column drag">
                                                <input
                                                    type="range"
                                                    value={phase.value}
                                                    min={0}
                                                    max={10}
                                                    step={0.25}
                                                    onChange={e => {
                                                        updateProjectPhase(
                                                            activeProject.id,
                                                            i,
                                                            'value',
                                                            e.target.value
                                                        )
                                                    }}
                                                />
                                            </div>
                                            <div className="column reset">
                                                <button>Reset</button>
                                            </div>
                                            <div className="column value">
                                                <span className="value">
                                                    {Math.floor(v * 100) / 100}{' '}
                                                    {v === 1 ? ' day' : ' days'}
                                                </span>
                                            </div>
                                            <div className="column  date start-date">
                                                <span className="start-date">
                                                    {start_date.format(
                                                        'MMMM Do'
                                                    )}
                                                </span>
                                            </div>
                                            <div className="column date ">
                                                <span className="end-date">
                                                    {d.format('MMMM Do')}
                                                </span>
                                            </div>
                                        </div>
                                    </Draggable>
                                )
                            })}
                        </Container>
                        {/* <AddPhase /> */}
                    </div>

                    <div className="results">
                        <h4>Ready for QA</h4>
                        <span className="important">{d.format('MMMM Do')}</span>
                    </div>
                    <div className="option">
                        <h4>Options</h4>
                        <button onClick={replaceInitialValues}>Reset</button>
                        <button
                            onClick={() => {
                                deleteProject(activeProject.id)
                            }}
                        >
                            Delete Project
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProjectEditor
