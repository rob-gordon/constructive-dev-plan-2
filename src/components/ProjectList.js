import React from 'react';

const ProjectList = props => {
    return (
        <div className="container">
            <ul className="main-project-list">
                {Object.keys(props.projects).map(x => {
                    return (
                        <li>
                            <button
                                onClick={e => {
                                    props.openProject(props.projects[x].id);
                                }}
                            >
                                {props.projects[x].siteTitle}
                            </button>
                        </li>
                    );
                })}
                <li>
                    <button
                        className="add-new-project"
                        onClick={e => {
                            props.makeNewProject();
                        }}
                    >
                        Add New Project
                    </button>
                </li>
            </ul>
        </div>
    );
};

export default ProjectList;
