import moment from 'moment';

const initialPhase = {
    startDate: moment().format(),
    complexity: 3,
    siteTitle: 'New Site',
    phases: [
        {
            title: 'Project Bootstrap',
            type: 'fixed',
            value: 0.5,
            active: true
        },
        {
            title: 'Site Plan',
            type: 'fixed',
            value: 1,
            active: true
        },
        {
            title: 'Menus & Routes',
            type: 'relative',
            value: 3,
            active: true
        },
        {
            title: 'Setting Up Admin Fields',
            type: 'relative',
            value: 6,
            active: true
        },
        {
            title: 'Migration',
            type: 'relative',
            value: 3,
            active: true
        },
        {
            title: 'Colors',
            type: 'fixed',
            value: 0.5,
            active: true
        },
        {
            title: 'Container',
            type: 'fixed',
            value: 0.5,
            active: true
        },
        {
            title: 'Sourcing Typography',
            type: 'fixed',
            value: 0.5,
            active: true
        },
        {
            title: 'Typography',
            type: 'relative',
            value: 3,
            active: true
        },
        {
            title: 'Rows',
            type: 'relative',
            value: 12,
            active: true
        },
        {
            title: 'Integration',
            type: 'fixed',
            value: 3,
            active: true
        }
    ]
};

export default initialPhase;
