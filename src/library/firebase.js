import firebase from 'firebase';

// Initialize Firebase
var config = {
    apiKey: 'AIzaSyBHep1OTamsSZ9gvjQZ2_wp9KVZ7-_xBZw',
    authDomain: 'constructive-dev-plan.firebaseapp.com',
    databaseURL: 'https://constructive-dev-plan.firebaseio.com',
    projectId: 'constructive-dev-plan',
    storageBucket: 'constructive-dev-plan.appspot.com',
    messagingSenderId: '748681287023'
};
firebase.initializeApp(config);
export default firebase;
